#!/bin/bash
################################################################################
#
# Filename: autostart.zsh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-28
#
################################################################################
#set -x

PATH_TO_MKDOCS_DOCS="${HOME}/b2c/my-own/distributor/doc"
PATH_TO_VIRTUALBOX_SHARED_FOLDER="${HOME}/virtualbox-tmp"

# On se déplace dans le dossier contenant les scripts pour que
# source ./libsh fonctionne.
cd ${HOME}/b2c/my-own/script/

# Tmux


# Mkdocs server
${HOME}/b2c/my-own/script/autostart_mkdocs.sh $PATH_TO_MKDOCS_DOCS

# Google webbrowser
${HOME}/b2c/my-own/script/autostart_google.sh

# VirtualBox shared folder mount
${HOME}/b2c/my-own/script/autostart_shared_folder.sh $PATH_TO_VIRTUALBOX_SHARED_FOLDER

# Après s'être déplacer cd le dossier contenant les scripts,
# on revient dans le home pour l'utilisateur.
cd ${HOME}

#set +x
#exit 0
