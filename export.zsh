#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-28
#
################################################################################
#set -x

export EDITOR='vim'
export VISUAL='vim'

#set +x
