#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-28
#
################################################################################
#set -x

# [BINARY]
alias  mkd="$HOME/.local/bin/mkdocs"

# [CONFIGURATION]
alias  zrc="vim ${HOME}/.zshrc"
alias   za="vim ${HOME}/b2c/my-own/zsh/alias.zsh"
alias  zaa="vim ${HOME}/b2c/my-own/zsh/autostart.zsh"

# [DISTRIBUTEUR]
alias  ddp='ssh distrib-dev "poweroff"'
alias  ddr='ssh distrib-dev "reboot"'
alias  dde='ssh distrib-dev "cd /home/; ./DISTRIBUTEUR"'
alias ddla='ssh distrib-dev "tail -f /var/log/appliLog.txt"'
alias ddld='ssh distrib-dev "tail -f /var/log/dbgLog.txt"'
alias ddlp='ssh distrib-dev "tail -f /var/log/paramLog.txt"'

# [EDITOR]
alias    v="vim"
alias    e="emacs"

# [ORGANISATION]
alias    ta='task add'
alias    tl='task list'
alias   tca='task calendar'

# [NAVIGATION]
alias   dsk="cd ${HOME}/Desktop"
alias   doc="cd ${HOME}/Documents"
alias   dow="cd ${HOME}/Downloads"
alias   mus="cd ${HOME}/Music"
alias   pct="cd ${HOME}/Pictures"
alias   vid="cd ${HOME}/Videos"
alias   vbt="cd ${HOME}/virtualbox-tmp"

alias    b2="cd ${HOME}/b2c"
alias    bm="cd ${HOME}/b2c/my-own"

alias   bmd="cd ${HOME}/b2c/my-own/distributor"
alias  bmdd="cd ${HOME}/b2c/my-own/distributor/diagram"
alias  bmdo="cd ${HOME}/b2c/my-own/distributor/doc"
alias  bmdp="cd ${HOME}/b2c/my-own/distributor/param"
alias  bmds="cd ${HOME}/b2c/my-own/distributor/source-1.26"

alias   bml="cd ${HOME}/b2c/my-own/libsh"
alias   bmp="cd ${HOME}/b2c/my-own/prezto"
alias   bms="cd ${HOME}/b2c/my-own/script"
alias   bmz="cd ${HOME}/b2c/my-own/zsh"


alias   cdo="cd ${HOME}/b2c/my-own/distributor/doc"
alias   cdu="cd ${HOME}/b2c/my-own/distributor/util"
alias   cds="cd ${HOME}/b2c/my-own/script"
alias   cdl="cd ${HOME}/b2c/my-own/libsh"

alias    bi="cd ${HOME}/b2c/in-group"

alias    bb="cd ${HOME}/b2c/by-other"
alias     r='ranger'

# [SCRIPT]
alias   nbd="${HOME}/b2c/my-own/script/new_binary_to_distributeur.sh"

# [SYSTEM]
alias     x='exit'
alias     j='jobs'
alias  mvbt="sudo mount -t vboxsf -o uid=${UID},gid=$(id -g) tmp ${HOME}/virtualbox-tmp"
alias  skus='setxkbmap -layout us'
alias  skui='setxkbmap -layout us -variant intl'
alias  skfr='setxkbmap -layout fr'
alias    tm='tmux'
alias   acs='apt-cache search'
alias saguu='sudo apt-get update && sudo apt-get upgrade -y'
alias sagir='sudo apt-get install --install-recommends'
alias sagar='sudo apt-get autoremove'
alias sagif='sudo apt-get install -f'
alias   psg='ps -aux | grep -v grep | grep -i'

# [TEST]
alias    sb='ssh -T git@bitbucket.org'

# [WATCH]
alias     h='sudo htop'
alias     f='fg %'
alias     t='tree -L'
alias    td="vim ${HOME}/b2c/.todo"
alias   pyg='pygmentize'
alias  cred="cat ${HOME}/b2c/.cred"
alias  note="cat ${HOME}/b2c/.note"
alias   url="cat ${HOME}/b2c/.url"
alias  util="cat ${HOME}/b2c/.util"
alias   llg='ls -l | grep -i'
alias   lag='ls -la | grep -i'

#set +x
